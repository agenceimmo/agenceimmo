#agenceImmo 

agenceImmo est un site internet qui gère les transactions immobilières de ses clients, de la mise en vente de biens à la gestion locative, en passant par l’estimation ou la recherche de biens. Ses collaborateurs sont généralement des agents immobiliers.